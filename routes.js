
async function routes (fastify, options) {

  fastify.log.info(`🤖 options routes:`)
  fastify.log.info(options)

  fastify.get(`/hello`, async (request, reply) => {
    return {
      options: options
    }
  })
}

module.exports = routes
