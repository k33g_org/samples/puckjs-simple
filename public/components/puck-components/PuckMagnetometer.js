import SmallElement from '../SmallElement.js'

export default class PuckMagnetometer extends SmallElement {
  styleSheets() { return [window.bulma] }

  html() { return `
    <div>
     <button class="button is-warning">
       🧭 get magnetometer values
     </button>
     <button class="button is-warning">
       🧭 listen to magnetometer
     </button>
     <button class="button is-danger">stop</button>
      <hr>
      <div class="title is-5">values: <values></values></div>
    </div>
  `}

  updateValues(values) {

    this.$('values').innerText = `x:${values.x} y:${values.y} z:${values.z}`
  }

  getXYZ() {
    Puck.eval(`{result:Puck.mag()}`, (data, error) => {
      if(data) {
        this.updateValues(data.result)
      }
      if(error) {
        console.log(error)
      }
    })
  }

  initialize() {
    this.updateValues({x:0,y:0,z:0})

    let listener = null

    this.$all('button')[0].addEventListener('click', event => {
      this.getXYZ()
    })

    this.$all('button')[1].addEventListener('click', event => {
      listener = setInterval(function() { this.getXYZ() }.bind(this), 500)
    })

    this.$all('button')[2].addEventListener('click', event => {
      clearInterval(listener)
    })

  }

}

customElements.define('puck-magnetometer', PuckMagnetometer)

