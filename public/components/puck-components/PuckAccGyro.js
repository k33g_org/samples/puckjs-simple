import SmallElement from '../SmallElement.js'

export default class PuckAccGyro extends SmallElement {
  styleSheets() { return [window.bulma] }

  html() { return `
    <style>
    .wrapper {
        min-height: 400px;
        padding: 16px 0;
        position: relative;
    }
    </style>
    <div>
     <button class="button is-dark">
      💫 listen to accelerometer/gyro
     </button>
     <button class="button is-danger">stop</button>
      <hr>
      <div class="title is-5">values: <values></values></div>

      <div class="field is-grouped is-grouped-multiline">
        <div class="control">
          <div class="tags has-addons">
            <span class="tag is-success is-medium">Accelerometer</span>
          </div>
        </div>

        <div class="control">
          <div class="tags has-addons">
            <span class="tag is-medium">x</span>
            <span id="acc-x" class="tag is-success is-medium"></span>
          </div>
        </div>

        <div class="control">
          <div class="tags has-addons">
            <span class="tag is-medium">y</span>
            <span id="acc-y" class="tag is-success is-medium"></span>
          </div>
        </div>

        <div class="control">
          <div class="tags has-addons">
            <span class="tag is-medium">z</span>
            <span id="acc-z" class="tag is-success is-medium"></span>
          </div>
        </div>

        <!-- -->

        <div class="control">
          <div class="tags has-addons">
            <span class="tag is-info is-medium">Gyro</span>
          </div>
        </div>

        <div class="control">
          <div class="tags has-addons">
            <span class="tag is-medium">x</span>
            <span id="gyro-x" class="tag is-info is-medium"></span>
          </div>
        </div>

        <div class="control">
          <div class="tags has-addons">
            <span class="tag is-medium">y</span>
            <span id="gyro-y" class="tag is-info is-medium"></span>
          </div>
        </div>

        <div class="control">
          <div class="tags has-addons">
            <span class="tag is-medium">z</span>
            <span id="gyro-z" class="tag is-info is-medium"></span>
          </div>
        </div>

      </div>


      <!--
      <div class="columns">
        <div class="column">
          <div class"wrapper">
            <div id="accChart" width="500" height="500" style="border:1px solid #d3d3d3;"></div>
          </div>
        </div>
        <div class="column">
          <div class"wrapper">
            <div id="gyroChart" width="500" height="500" style="border:1px solid #d3d3d3;"></div>
          </div>
        </div>
      </div>
      -->

    </div>
  `}

  updateValues(values) {
    this.$('values').innerText = JSON.stringify(values)

    this.accX.innerText = values.acc.x
    this.accY.innerText = values.acc.y
    this.accZ.innerText = values.acc.z

    this.gyroX.innerText = values.gyro.x
    this.gyroY.innerText = values.gyro.y
    this.gyroZ.innerText = values.gyro.z

  }

  getValues() {

    Puck.eval(`{result:Puck.accel()}`, (data, error) => {

      // values: {"acc":{"x":197,"y":1660,"z":6299},"gyro":{"x":-38,"y":-83,"z":210}}
      if(data==null || error) {
        console.log(data, error)
      } else {
        this.updateValues(data.result)
        console.log(data.result)
      }
    })

  }

  initialize() {
    //this.updateValues({})

    let listener = null

    this.accX = this.$("#acc-x")
    this.accY = this.$("#acc-y")
    this.accZ = this.$("#acc-z")

    this.gyroX = this.$("#gyro-x")
    this.gyroY = this.$("#gyro-y")
    this.gyroZ = this.$("#gyro-z")

    this.$all('button')[0].addEventListener('click', event => {
      listener = setInterval(function() { this.getValues() }.bind(this), 500)
    })

    this.$all('button')[1].addEventListener('click', event => {
      clearInterval(listener)
    })

  }

}

customElements.define('puck-acc-gyro', PuckAccGyro)

