const template = document.createElement('template')
template.innerHTML = `
  <div>
    <button class="button is-dark">🚀 Connect to BlueTooth device</button>
    <button class="button is-dark">📝 Get the list of BlueTooth devices</button>
    <button class="button is-dark">😴 Disconnect</button>
    <hr>
    <article class="message is-primary">
      <div class="message-header">
        <p>Output</p>
        <button class="delete" aria-label="delete"></button>
      </div>
      <div class="message-body">

      </div>
    </article>
  </div>
`

export default class BtConnect extends HTMLElement {

  log(message) {
    this.output.innerText+=`${message}\n`
  }

  requestBluetoothDevice() {
    this.log('Requesting any Bluetooth device...')
    navigator.bluetooth.requestDevice({
      // filters: [...] <- Prefer filters to save energy & show relevant devices.
      acceptAllDevices: true
    })
    .then(device => {
      this.log(`> Requested ${device.name} (${device.id})`)
    })
    .catch(error => {
      this.log(`😡 ${error}`)
    })
  }

  getBluetoothDevices() {
    this.log('Getting existing permitted Bluetooth devices...')
    navigator.bluetooth.getDevices()
    .then(devices => {
      this.log(`> Got ${devices.length} Bluetooth devices.`)
      devices.forEach(device => {
        this.log(`  > ${device.name} (${device.id})`)
      })
    })
    .catch(error => {
      this.log(`😡 ${error}`)
    })
  }

  unPairBluetoothDevices() {
    this.log('Getting existing permitted Bluetooth devices...')
    navigator.bluetooth.getDevices()
    .then(devices => {
      this.log(`> Got ${devices.length} Bluetooth devices.`)
      devices.forEach(device => {
        console.log(device)
        if(device.gatt.connected) {
          device.gatt.disconnect()
          this.log(`  > ${device.name} (${device.id}) 😴`)
        } else {
          this.log(`  > 🖐️ ${device.name} (${device.id}) already disconnected`)
        }


      })
    })
    .catch(error => {
      this.log(`😡 ${error}`)
    })
  }

  constructor() {
    super()
    this.attachShadow({mode: 'open'})
    this.shadowRoot.appendChild(template.content.cloneNode(true))
  }

  connectedCallback() {
    this.shadowRoot.adoptedStyleSheets = [window.bulma]
    this.buttons = this.shadowRoot.querySelectorAll("button")

    this.output = this.shadowRoot.querySelector(`.message-body`)

    this.buttons[0].addEventListener('click', event => {
      console.log("connect")
      this.requestBluetoothDevice()
    })
    this.buttons[1].addEventListener('click', event => {
      console.log("list")
      this.getBluetoothDevices()
    })
    this.buttons[2].addEventListener('click', event => {
      console.log("disconnect")
      // this is a WIP 🚧
      this.unPairBluetoothDevices()
    })
  }
}

customElements.define('bt-connect', BtConnect)
