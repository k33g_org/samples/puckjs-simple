import SmallElement from '../SmallElement.js'

export default class PixlScreen extends SmallElement {
  styleSheets() { return [window.bulma] }

  html() { return `
    <div>
        <!--
        <div class="field">
            <div class="control">
                <textarea class="textarea is-primary" placeholder="Primary textarea"></textarea>
            </div>
        </div>
        -->

        <div class="field is-horizontal mt-2">
          <div class="control mr-2">
            <input class="input is-primary" name="x" type="text" placeholder="X">
          </div>
          <div class="control mr-2">
            <input class="input is-primary" name="y" type="text" placeholder="Y">
          </div>
          <div class="control mr-2">
            <input class="input is-primary" name="message" type="text" placeholder="Message 📝">
          </div>
        </div>
        <button class="button is-dark mt-2">Clear Screen</button>
        <button class="button is-dark mt-2">Display</button>
        <button class="button is-primary mt-2"></button>

    </div>
  `}

  initialize() {
    var status = "off"
    let changeButtonText = (status) => this.$all('button')[2].innerText = `💡 ${status}`

    changeButtonText(status)

    this.$all('button')[2].addEventListener('click', event => {
      if(status=="off") {
        Puck.write(`LED1.set();\n`, data => {
          console.log("🤖", data)
          status = "on"
          changeButtonText(status)
          this.$all('button')[2].className = "button is-warning mt-2"
        })
      } else {
        Puck.write(`LED1.reset();\n`, data => {
          console.log("🤖", data)
          status = "off"
          changeButtonText(status)
          this.$all('button')[2].className = "button is-primary mt-2"
        })
      }
    })

    this.$all('button')[0].addEventListener('click', event => {
      // initialize
      let cmds = [
        `g.clear()`,
        `g.flip()`
      ]
      cmds.forEach(cmd => {
        Puck.eval(`{initScreen: ${cmd}}`, (data, error) =>{
          console.log(cmd, { error, data})
        })
      })
    })

    this.$all('button')[1].addEventListener('click', event => {
      //console.log(this.$name("x").value, this.$name("y").value, this.$name("message").value)

      let cmds = [
        `g.setFontVector(g.getHeight()/3)`,
        `g.drawString("${this.$name('message').value}", ${this.$name('x').value}, ${this.$name('y').value})`,
        `g.flip()`
      ]
      cmds.forEach(cmd => {
        Puck.eval(`{display: ${cmd}}`, (data, error) => {
          console.log(cmd, { error, data})
        })
      })

    })

  }

}

customElements.define('pixl-screen', PixlScreen)
