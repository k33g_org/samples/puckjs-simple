import SmallElement from '../SmallElement.js'

export default class PuckColorLed extends SmallElement {
  styleSheets() { return [window.bulma] }
  /*
    ### Questions
    - how to check if a led is on or off?
  */
  html() { return `
    <button class="button is-primary"></button>
  `}

  leds() {
    return {
      red: {picto: "🔴", id: 1},
      green: {picto: "🟢", id: 2},
      blue: {picto: "🔵", id: 3}
    }
  }

  initialize() {
    let color = this.getAttribute("color")
    let picto = this.leds()[color].picto
    let id = this.leds()[color].id

    let changeButtonText = (picto, status) => this.$('button').innerText = `${picto} ${status}`
    var status = "off"

    changeButtonText(picto, status)

    this.$('button').addEventListener('click', event => {
      if(status=="off") {
        Puck.write(`LED${id}.set();\n`, data => {
          console.log("🤖", data)
          status = "on"
          changeButtonText(picto, status)
          this.$('button').className = "button is-warning"
        })
      } else {
        Puck.write(`LED${id}.reset();\n`, data => {
          console.log("🤖", data)
          status = "off"
          changeButtonText(picto, status)
          this.$('button').className = "button is-primary"
        })
      }
    })
  }

}
customElements.define('puck-color-led', PuckColorLed)
