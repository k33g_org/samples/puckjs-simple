import SmallElement from '../SmallElement.js'

export default class PuckGetTemperature extends SmallElement {
  styleSheets() { return [window.bulma] }

  html() { return `
    <div>
      <button class="button is-danger">🌡 get temperature value</button>
      <hr>
      <div class="title is-5">value: <value></value></div>

    </div>
  `}

  initialize() {
    this.$('value').innerText = 0

    this.$('button').addEventListener('click', event => {

      Puck.eval("Puck.getTemperature()", (value) => {
        console.log("🤖 temperature:", value)
        this.$('value').innerText = value
      })

    })
  }

}
customElements.define('puck-get-temperature', PuckGetTemperature)
