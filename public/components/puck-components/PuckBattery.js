import SmallElement from '../SmallElement.js'

export default class PuckBattery extends SmallElement {
  styleSheets() { return [window.bulma] }

  html() { return `
    <div>
      <button class="button is-dark">🔋 get battery level</button>
      <hr>
      <div class="title is-5">value: <value></value></div>
    </div>
  `}

  initialize() {
    this.$('value').innerText = "0 %"

    this.$('button').addEventListener('click', event => {
      Puck.eval(`Puck.getBatteryPercentage()`, (data, error) => {
        this.$('value').innerText = `${data} %`
      })
    })

  }

}

customElements.define('puck-battery', PuckBattery)
