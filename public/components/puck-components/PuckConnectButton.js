import SmallElement from '../SmallElement.js'

export default class PuckConnectButton extends SmallElement {
  styleSheets() { return [window.bulma] }
  /*
    ### Why this components?
    Web Bluetooth implementations can only connect to a Bluetooth LE device **in response to user input**
  */
  html() { return `
    <div>
      <button class="button is-dark">🚀 Connect</button>
    </div>
  `}

  initialize() {
    this.$('button').addEventListener('click', event => {
      Puck.write('LED1.reset();\n', data => {
        console.log("🤖", data)
        this.$('button').innerText = "😃 Connected"

        this.$('button').className = "button is-success"

      })
    })



  }
}
customElements.define('puck-connect-button', PuckConnectButton)
