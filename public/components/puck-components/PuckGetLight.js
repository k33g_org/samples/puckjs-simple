import SmallElement from '../SmallElement.js'

export default class PuckGetLight extends SmallElement {
  styleSheets() { return [window.bulma] }

  html() { return `
    <div>
      <button class="button is-info">💡 get light value:</button>
      <hr>
      <div class="title is-5">value: <value></value></div>
    </div>
  `}

  initialize() {
    this.$('value').innerText = 0

    this.$('button').addEventListener('click', event => {

      Puck.eval("Puck.light()", (value, error) => {
        console.log("🤖 light:", value)
        console.log("😡 light:", error)

        this.$('value').innerText = value === null ? `😡 ${error}` : value
      })
    })

  }

}
customElements.define('puck-get-light', PuckGetLight)
