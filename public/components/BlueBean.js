

class BlueBean {
  constructor() {

  }

  devices() {
    let options = {}

    let request = (options) => navigator.bluetooth.requestDevice(options)

    return {
      all: () => {
        options.acceptAllDevices = true
        return request(options)
      },
      namePrefix: (prefix) => {
        options.filters=[{namePrefix:prefix}]
        return request(options)
      }
    }
  }

}

export { BlueBean }
