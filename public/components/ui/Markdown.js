import SmallElement from '../SmallElement.js'

export default class Markdown extends SmallElement {

  styleSheets() { return [window.highlight, window.purple] }

  html() { return `
    <div>
      <h1>⏳ loading...</h1>
    </div>
  `}

  initialize() {
    //console.log("🎃", this.getAttribute("file"))
    fetch(this.getAttribute("file"))
      .then(response => response.text())
      .then(mdText => {
        //console.log(mdText)
        this.setHtml(window.markdownit().render(mdText))

        this.$all('code').forEach(code => {
          //console.log(code.style)
          code.style.fontFamily="Menlo"
          hljs.highlightBlock(code)
        })
      })

  }

}

customElements.define('mark-down', Markdown)
