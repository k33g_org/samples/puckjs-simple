import SmallElement from '../SmallElement.js'

/*
let slides = [
  {id:"hello", title:"Hello", display:"block", status:"is-active", component:"../../slides/01-hello.js", tag:"slide-1"},
  {id:"plan", title:"Plan", display:"none", status:"", component:"../../slides/02-plan.js", tag:"slide-2"},
  {id:"intro", title:"Intro", display:"none", status:"", component:"../../slides/03-intro.js", tag:"slide-3"},
  {id:"connect", title:"Connect", display:"none", status:"", component:"../../slides/04-connection.js", tag:"slide-4"},
  {id:"leds", title:"Leds", display:"none", status:"", component:"../../slides/05-color-leds.js", tag:"slide-5"}
]
*/

import { slides } from '../../slides/slides.js'

let tabsBar = (slides) => `
    <!-- Tabs -->
    <div class="tabs">
      <ul>
        ${slides.map(row => `
          <li class="tab ${row.status}">
            <a slide="${row.id}">${row.title}</a>
          </li>
        `).join("")}
      </ul>
    </div>
    <!-- End of Tabs -->
`

let slidesSection = (slides) => `
    <!-- Slides -->
    <div class="container section" style="padding-top:1px">
      ${slides.map(row => `
        <div id="${row.id}" class="content-tab" style="display:${row.display}">
          <!--here my content: ${row.title}-->
          <${row.tag}></${row.tag}>
        </div>
      `).join("")}

    </div>
    <!-- End of Slides -->
`

export default class SmallDeck extends SmallElement {

  styleSheets() {
    return [window.bulma]
  }

  html() { return `
    <h1>⏳ loading...</h1>
  `}

  manageEvents() {
    let allTabs = this.$classes('tab')

    allTabs.forEach(tab => {

      tab.addEventListener('click', event => {

        // === clicked tab ===
        let targetElement = event.srcElement
        let currentSlide = targetElement.getAttribute("slide")
        //console.log("click on tab: ", currentSlide)

        allTabs.forEach(tab => {
          tab.className = "tab"
         })
        // currentTarget ???
        event.currentTarget.className += " is-active"

        // === content tabs ===
        this.$classes('content-tab').forEach(contentTab => {
          //console.log("👋 display:", "slideId:",currentSlide, "id:", contentTab.getAttribute("id"), "display:", contentTab.style.display)
          if(contentTab.getAttribute("id")==currentSlide) {
            contentTab.style.display="block"
          } else {
            contentTab.style.display="none"
          }
        })
      })
    })
  }

  initialize() {

    // array of promises
    // load slides components
    Promise.all(slides.map(row => import(row.component))).then(loadedSlides => {
      console.log("🖐️", loadedSlides)
      // update main component
      this.setHtml(`
        ${tabsBar(slides)}
        ${slidesSection(slides)}
      `)
      this.manageEvents()


    }).catch(error => {
      console.log("😡", error)
      this.setHtml(this.html()+"😡 "+error)
    })

  }

}

customElements.define('small-deck', SmallDeck)
