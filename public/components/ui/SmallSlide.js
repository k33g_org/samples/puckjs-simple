import SmallElement from '../SmallElement.js'
import Markdown from './Markdown.js'

export default class SmallSlide extends SmallElement {

  styleSheets() { return [window.bulma] }
  //styleSheets() { return [window.highlight] }

  html() { return `
    <h1>⏳ loading...</h1>
  `}

  initialize() {

  }

}
