export default class SmallElement extends HTMLElement {

  html() { return `
    <div>👋 hello world 🌍</div>
  `}

  $(selector) {
    return this.shadowRoot.querySelector(selector)
  }

  $class(name) {
    return this.shadowRoot.querySelector(`.${name}`)
  }

  $classes(name) {
    return this.shadowRoot.querySelectorAll(`.${name}`)
  }

  $name(name) {
    return this.shadowRoot.querySelector(`[name="${name}"]`)
  }

  $id(id) {
    return this.shadowRoot.getElementById(id)
  }

  $all(selector) {
    return this.shadowRoot.querySelectorAll(selector)
  }

  initialize() {
    console.log("🤖 initialize component")
  }

  styleSheets() {
    return []
  }

  setHtml(htmlContent) {
    this.shadowRoot.innerHTML = htmlContent
  }

  connectedCallback() {
    this.attachShadow({mode: 'open'})
    this.shadowRoot.adoptedStyleSheets = this.styleSheets()
    this.shadowRoot.innerHTML = this.html()
    this.initialize()
  }
}
