# Espruino, Puck, ... ???

## Espruino

### Espruino: Interpréteur JavaScript 😍 pour Micro contrôleur

### Espruino board(s): Micro contrôleur programmable en JavaScript 🎉
Plusieurs versions
- [Original](https://www.espruino.com/Original) [Pico](https://www.espruino.com/Pico)
- [Wifi](https://www.espruino.com/WiFi) à base d'ESP8266
- Les **BLE** [Puck](https://www.espruino.com/Puck.js) 💙 [Pixl](https://www.espruino.com/Pixl.js) 💙 [MDBT42Q](https://www.espruino.com/MDBT42Q) [Bangle](https://www.espruino.com/Bangle.js)
- Autres boards: ex: [RUUVITAG](https://ruuvi.com/ruuvitag-specs/) [Thingy:52](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_thingy52%2FUG%2Fthingy52%2Fkit_content%2Fhw_kit_content.html)

![all6](./slides/all6.jpg)

## Pourquoi le Puck ?

- JavaScript
- "tout prêt"
- facile à utiliser
- nombreux capteurs
- **BLE**


## Je suis en mode "découverte"

- 3 modes d'interactions:
  - 🌍 navigateur ⬅️ 🖐️ *avec la web API BlueTooth*
  - 🖥 nodejs, python (*)
  - 📟 dans le microprocesseur avec le [Espruino IDE](https://www.espruino.com/ide/) 🖐️ **DEMO** 🖐️

> (*) avec un RPI si vous avez un Mac 😛

## Exigences techniques

- 🖐️ https (générer des certificats https "locally-trusted": [mkcert](https://github.com/FiloSottile/mkcert))
  - GitPod 😍
  - GitLab Pages
  - GitHub Pages
- Chrome (et Chrome Beta si vous êtes sur BigSur 😢) *Version 88.0.4324.41 (Official Build) beta (x86_64)* sinon Opera et Edge
