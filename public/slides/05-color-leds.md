
### Allumer une LED

```javascript
Puck.write(`LED${id}.set();\n`, data => {
  console.log("🤖", data)
})
```

### Eteindre une LED

```javascript
Puck.write(`LED${id}.reset();\n`, data => {
  console.log("🤖", data)
})
```

### Remarques

> - 👋 🔴 => id = 1
> - 👋 🟢 => id = 2
> - 👋 🔵 => id = 3
