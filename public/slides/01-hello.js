import SmallSlide from '../components/ui/SmallSlide.js'

export default class Slide1 extends SmallSlide {
  html() { return `<mark-down file="./slides/01-hello.md"></mark-down>`}
  initialize() {}
}

customElements.define('slide-1', Slide1)
