

## Connexion dès la 1ère **commande**

J'utilise la librairie **puck.js**:

```html
<script src="https://puck-js.com/puck.js"></script>
```

> - 🖐️ votre web app doit être en https
> - le tuto officiel: [https://www.espruino.com/Web%20Bluetooth](https://www.espruino.com/Web%20Bluetooth)


```javascript
Puck.write('LED1.reset();\n', data => {
  console.log("🤖", data)
})
```

## Connexion "bis"

### Se connecter à un device
```javascript
navigator.bluetooth.requestDevice({
  // filters: [...] <- Prefer filters to save energy & show relevant devices.
  acceptAllDevices: true
})
.then(device => {
  console.log(`> Requested ${device.name} (${device.id})`)
})
.catch(error => {
  console.log(`😡 ${error}`)
})
```

### Avoir la liste des devices "permis" (paired?)
```javascript
navigator.bluetooth.getDevices()
.then(devices => {
  console.log(`> Got ${devices.length} Bluetooth devices.`)
  devices.forEach(device => {
    console.log(`  > ${device.name} (${device.id})`)
  })
})
.catch(error => {
  console.log(`😡 ${error}`)
})
```

### Annuler le pairing avec un device
# 🤔 🤬
