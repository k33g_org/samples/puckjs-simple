# That's (almost) all folks 👋

## 🤔 Les questions que je me pose/posais

#### Comment je sécurise un device ?

- [http://forum.espruino.com/conversations/298911/](http://forum.espruino.com/conversations/298911/)
  - => [http://forum.espruino.com/conversations/297465/#comment13400243](http://forum.espruino.com/conversations/297465/#comment13400243)

```javascript
var serverAddress="xx:xx:xx:xx:xx:xx";
NRF.on('connect',function(addr) {
  if (addr.split(' ')[0]!=serverAddress) {
    NRF.disconnect();
  }
});
```

#### Comment je change l'ID d'un device ?

- [Change device name without overriding Espruino services](http://forum.espruino.com/conversations/297409/)
- [HOWTO Change Espruino Bluetooth name and passkey](http://forum.espruino.com/conversations/454/)


#### Comment j'utilise plusieurs devices en même temps avec la BT Web API ?

- Je pense que ce n'est pas possible, mais je continue de creuser
  - 15/12/2020 (update) d'après le header de la lib **puck.js** il semblerait que ce soit possible 🚧
    - cf. démo du début

## Outils

- [https://github.com/espruino/EspruinoTools](https://github.com/espruino/EspruinoTools) *(ne fonctionne pas avec BigSur)*
- [Espruino Hub (BLE => MQTT)](https://github.com/espruino/EspruinoHub)
- [S'interfacer avec un PC/MAC](https://www.espruino.com/Interfacing#bluetooth-le)

## One more thing: Thingy

> +/- compatible avec Espruino

- [Nordic Thingy:52](https://www.nordicsemi.com/Software-and-tools/Prototyping-platforms/Nordic-Thingy-52/GetStarted)
- [Lib JavaScript](https://github.com/NordicPlayground/Nordic-Thingy52-Thingyjs) 😍 très complète, très bien documentée
- [Demo](https://developer.nordicsemi.com/thingy/52)
  - [Webapps Polymer et React](https://github.com/NordicPlayground/webapp-nordic-thingy)
- [Outils](https://www.nordicsemi.com/Software-and-tools/Development-Tools)
- [SDK Android](https://github.com/NordicSemiconductor/Android-Nordic-Thingy)
- [SDK IOS](https://github.com/NordicSemiconductor/IOS-Nordic-Thingy)


## Des références

- Specs [https://webbluetoothcg.github.io/web-bluetooth/](https://webbluetoothcg.github.io/web-bluetooth/)
- [2015] [https://web.dev/bluetooth/](https://web.dev/bluetooth/)
  - [https://googlechrome.github.io/samples/web-bluetooth/index.html](https://googlechrome.github.io/samples/web-bluetooth/index.html)
  - [https://github.com/googlechrome/samples/tree/gh-pages/web-bluetooth](https://github.com/googlechrome/samples/tree/gh-pages/web-bluetooth)
  - [https://www.chromestatus.com/feature/5264933985976320](https://www.chromestatus.com/feature/5264933985976320)

## Les projets que j'ai en tête

- Travailler sur la connexion entre un PI et le Puck et le Pixl
- Communication avec un M5Stack
- Télécommande à Slides
- Télécommande pour Bada
- Refaire l'application Thingy en "purs webcomponents"

## Merci 😍 encore

## Des questions ?
