import SmallSlide from '../components/ui/SmallSlide.js'
import PuckGetTemperature from '../components/puck-components/PuckGetTemperature.js'

export default class Slide6 extends SmallSlide {

  html() { return `
    <div>
      <h1 class="title">Temperature</h1>
      <hr>
      <puck-get-temperature></puck-get-temperature>
      <hr>
      <mark-down file="./slides/06-temperature.md"></mark-down>
    </div>
  `}

  initialize() {}

}

customElements.define('slide-6', Slide6)
