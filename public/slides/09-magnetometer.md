### 🧭 Boussole

```javascript
updateValues(values) {
  console.log(`${values.x} ${values.y} ${values.z}`)
}

// data.result
getXYZ() {
  Puck.eval(`{result:Puck.mag()}`, (data, error) => {
    this.updateValues(data.result)
  })
}
```
