# Bonjour 👋

## Merci 😍

## Philippe Charrière

### TAM @ GitLab 🦊

### 🐦 @k33g_org

### 📝 Blog(s)

#### ⏺ [https://k33g_org.gitlab.io/](https://k33g_org.gitlab.io/) 🖐️

#### ⏺ [https://k33g.gitlab.io/](https://k33g.gitlab.io/)

#### ⏺ [http://k33g.github.io/](http://k33g.github.io/)
