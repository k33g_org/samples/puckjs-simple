import SmallSlide from '../components/ui/SmallSlide.js'

export default class Slide3 extends SmallSlide {
  html() { return `<mark-down file="./slides/03-intro.md"></mark-down>`}
  initialize() {}
}

customElements.define('slide-3', Slide3)
