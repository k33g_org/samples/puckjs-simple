import SmallSlide from '../components/ui/SmallSlide.js'
import PuckBattery from '../components/puck-components/PuckBattery.js'

export default class Slide8 extends SmallSlide {

  html() { return `
    <div>
      <h1 class="title">Light</h1>
      <hr>
      <puck-battery></puck-battery>
      <hr>
      <mark-down file="./slides/08-battery.md"></mark-down>
    </div>
  `}

  initialize() {}

}

customElements.define('slide-8', Slide8)
