import SmallSlide from '../components/ui/SmallSlide.js'
import PuckAccGyro from '../components/puck-components/PuckAccGyro.js'

export default class Slide10 extends SmallSlide {

  html() { return `
    <div>
      <h1 class="title">Accelerometer / Gyroscope</h1>
      <hr>
      <puck-acc-gyro></puck-acc-gyro>
      <hr>
      <mark-down file="./slides/10-accgyro.md"></mark-down>
    </div>
  `}

  initialize() {}

}

customElements.define('slide-10', Slide10)
