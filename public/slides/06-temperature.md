### Obtenir la température ambiante

```javascript
Puck.eval("Puck.getTemperature()", (value) => {
  console.log("🤖 temperature:", value)
})
```
