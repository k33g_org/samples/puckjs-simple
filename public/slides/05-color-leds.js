import SmallSlide from '../components/ui/SmallSlide.js'
import PuckConnectButton from '../components/puck-components/PuckColorLed.js'

export default class Slide5 extends SmallSlide {

  html() { return `
    <div>
      <h1 class="title">Leds</h1>
      <hr>
      <puck-color-led color="red"></puck-color-led>
      <puck-color-led color="green"></puck-color-led>
      <puck-color-led color="blue"></puck-color-led>
      <hr>
      <mark-down file="./slides/05-color-leds.md"></mark-down>
    </div>
  `}

  initialize() {}

}

customElements.define('slide-5', Slide5)
