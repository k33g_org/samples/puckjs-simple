import SmallSlide from '../components/ui/SmallSlide.js'
import PixlScreen from '../components/puck-components/PixlScreen.js'

export default class Slide11 extends SmallSlide {

  html() { return `
    <div>
      <h1 class="title">Pixl.js</h1>
      <h2>🖐️ recharger la page</h2>
      <hr>
      <pixl-screen></pixl-screen>
      <hr>
      <mark-down file="./slides/11-pixl.md"></mark-down>
    <div>
  `}

  initialize() {}
}

customElements.define('slide-11', Slide11)
