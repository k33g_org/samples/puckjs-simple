import SmallSlide from '../components/ui/SmallSlide.js'
import PuckGetLight from '../components/puck-components/PuckGetLight.js'

export default class Slide7 extends SmallSlide {

  html() { return `
    <div>
      <h1 class="title">Light</h1>
      <hr>
      <puck-get-light></puck-get-light>
      <hr>
      <mark-down file="./slides/07-light.md"></mark-down>
    </div>
  `}

  initialize() {}

}

customElements.define('slide-7', Slide7)
