import SmallSlide from '../components/ui/SmallSlide.js'

export default class Slide12 extends SmallSlide {
  html() { return `<mark-down file="./slides/12-the-end.md"></mark-down>`}
  initialize() {}
}

customElements.define('slide-12', Slide12)
