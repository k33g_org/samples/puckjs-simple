const slides = [
  {id:"hello", title:"Hello", display:"block", status:"is-active", component:"../../slides/01-hello.js", tag:"slide-1"},
  //{id:"plan", title:"Plan", display:"none", status:"", component:"../../slides/02-plan.js", tag:"slide-2"},
  {id:"intro", title:"Intro", display:"none", status:"", component:"../../slides/03-intro.js", tag:"slide-3"},
  {id:"connect", title:"Connect", display:"none", status:"", component:"../../slides/04-connection.js", tag:"slide-4"},
  {id:"leds", title:"Leds", display:"none", status:"", component:"../../slides/05-color-leds.js", tag:"slide-5"},
  {id:"temperature", title:"Temperature", display:"none", status:"", component:"../../slides/06-temperature.js", tag:"slide-6"},
  {id:"light", title:"Light", display:"none", status:"", component:"../../slides/07-light.js", tag:"slide-7"},

  {id:"battery", title:"Battery", display:"none", status:"", component:"../../slides/08-battery.js", tag:"slide-8"},
  {id:"magnetometer", title:"Magnetometer", display:"none", status:"", component:"../../slides/09-magnetometer.js", tag:"slide-9"},
  {id:"accgyro", title:"Accelerometer/Gyroscope", display:"none", status:"", component:"../../slides/10-accgyro.js", tag:"slide-10"},

  {id:"pixl", title:"Pixl", display:"none", status:"", component:"../../slides/11-pixl.js", tag:"slide-11"},
  {id:"theEnd", title:"The End", display:"none", status:"", component:"../../slides/12-the-end.js", tag:"slide-12"}

]

export { slides }
