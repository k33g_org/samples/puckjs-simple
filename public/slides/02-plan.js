import SmallSlide from '../components/ui/SmallSlide.js'

export default class Slide2 extends SmallSlide {
  html() { return `<mark-down file="./slides/02-plan.md"></mark-down>`}
  initialize() {}
}

customElements.define('slide-2', Slide2)
