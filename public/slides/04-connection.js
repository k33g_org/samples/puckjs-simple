import SmallSlide from '../components/ui/SmallSlide.js'
import PuckConnectButton from '../components/puck-components/PuckConnectButton.js'
import BtConnect from '../components/puck-components/BtConnect.js'


export default class Slide4 extends SmallSlide {

  html() { return `
    <div>
      <h1 class="title">BT Connection</h1>
      <hr>
      <puck-connect-button></puck-connect-button>
      <hr>
      <mark-down file="./slides/04-connection.md"></mark-down>
      <hr>
      <bt-connect></bt-connect>
    </div>
  `}

  initialize() {

  }

}

customElements.define('slide-4', Slide4)
