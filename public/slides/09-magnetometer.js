import SmallSlide from '../components/ui/SmallSlide.js'
import PuckMagnetometer from '../components/puck-components/PuckMagnetometer.js'

export default class Slide9 extends SmallSlide {

  html() { return `
    <div>
      <h1 class="title">Magnetometer</h1>
      <hr>
      <puck-magnetometer></puck-magnetometer>
      <hr>
      <mark-down file="./slides/09-magnetometer.md"></mark-down>
    </div>
  `}

  initialize() {}

}

customElements.define('slide-9', Slide9)
