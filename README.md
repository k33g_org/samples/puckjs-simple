# puckjs-simple

## Refs

- https://www.espruino.com/Puck.js#tutorials
- https://www.espruino.com/Quick+Start+BLE#puckjs

Send individual JavaScript commands to Espruino without programming it:
- https://www.espruino.com/Quick+Start+BLE#sending-individual-commands
- Tutorial: https://www.espruino.com/Web%20Bluetooth

## Remarks

Can I send commands to Puck.js as soon as the page loads?
Unfortunately not - as a security precaution, Web Bluetooth implementations can only connect to a Bluetooth LE device **in response to user input**.
After that you can do what you want though.


## TODO

- Magnetometer
- Pixl

## To read

https://web.dev/bluetooth/
https://googlechrome.github.io/samples/web-bluetooth/device-info.html?namePrefix=Puck

